<?php
namespace wms\admin\components;

use Yii;
use wms\admin\ModuleAsset;

/**
 * Default controller
 */
class Controller extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        ModuleAsset::register($this->view);
    }
}
