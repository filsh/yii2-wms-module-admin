<?php

namespace wms\admin;

class ModuleAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@wms/admin/assets';
    
    public $css = [
        'css/main.css',
    ];
    
    public $js = [
        'js/main.js'
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
    
    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}
