/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Modules
// -----------------------------------------------------------------------------
window.wms = window.wms || {};

wms.formatters = (function($) {
    var pub = {
        select2package: function (repo) {
            if (repo.loading) {
                return repo.text;
            }
            var markup =
                '<div class="row">' + 
                    '<div class="col-sm-5">' +
                        '<b style="margin-left:5px">' + repo.package + '</b>' + 
                    '</div>' +
                    '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + repo.downloads + '</div>' +
                    '<div class="col-sm-3"><i class="fa fa-star"></i> ' + repo.favers + '</div>' +
                '</div>';
        
            if (repo.description) {
              markup += '<h5>' + repo.description + '</h5>';
            }
            return '<div style="overflow:hidden;">' + markup + '</div>';
        }
    };
    
    return pub;
})(window.jQuery);