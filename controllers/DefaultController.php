<?php
namespace wms\admin\controllers;

use Yii;
use wms\admin\components\Controller;

/**
 * Default controller
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeAction(\yii\base\Action $action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        
        if (Yii::$app->user->getIsGuest()) {
            $action->controller->layout = '//blank';
        }
        return true;
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
}
