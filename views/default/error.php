<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$params = [
    'name' => $name,
    'message' => $message,
    'exception' => $exception
];

if($exception instanceof \yii\web\HttpException) {
    switch($exception->statusCode) {
        case 404:
            echo $this->render('./errors/404', $params);
            break;
        case 500:
            echo $this->render('./errors/500', $params);
            break;
        default:
            echo $this->render('./errors/default', $params);
    }
} else {
    echo $this->render('./errors/default', $params);
}
