<?php

namespace wms\admin;

use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

class Module extends \wms\base\Module implements BootstrapInterface
{
    const VERSION = '1.0.0-dev';
    
    public function init()
    {
        parent::init();
        
        if (!isset(Yii::$app->i18n->translations['wms/admin*'])) {
            Yii::$app->i18n->translations['wms/admin*'] = [
                'class'    => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }

    public function bootstrap($app)
    {
    }
}